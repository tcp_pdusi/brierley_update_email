#!/bin/bash
set -x
myupddate=`date +"%Y-%m-%d"`
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  REPLY="${encoded}"   
}


export site=`cat brierley.conf|grep -i "^site" | sed 's/site=//'`
export clientId=`cat brierley.conf|grep -i "^clientId" | sed 's/clientId=//'`
export clientSecret=`cat brierley.conf|grep -i "^clientSecret" | sed 's/clientSecret=//'`
export tcpSiteStr=`cat brierley.conf|grep -i "^tcpSiteStr" | sed 's/tcpSiteStr=//'`

#tcpSiteStr=${tcpSiteStr}
echo "$tcpSiteStr"
#exit 0

if [[  $(find brierley.key -mtime -1|wc -l ) -le 0  || $( cat brierley.key | grep "^accessToken" | wc -l) -le 0 ]]; then
curl -o output.txt -X POST "https://${site}:8443/${tcpSiteStr}/api/v1/auth/token?" -H 'Content-Type: application/json'  -d@- <<EOF
{"clientId": "${clientId}",  "clientSecret": "${clientSecret}"}

EOF

cat output.txt | awk -F"," '{print $3}' |  sed 's/"//g' > brierley.key
export authcheck=`cat output.txt |sed 's/^.*developerMessage":.*userMessage":"\(.*\)","moreInfo.*$/\1/'`
if [[ ! "${authcheck}" == "You have been successfully authorized." ]]; then
echo "Invalid credentials to get Auth Token; Please check Brierley response below"
cat output.txt
rm output.txt #For Security purposes;
exit -1
fi

rm output.txt #For Security purposes;
fi

export accessToken=`cat brierley.key | grep "^accessToken" | sed 's/accessToken://'`
#echo "accessToken = ${accessToken}"
export myval=0
while read p; do
export checkVal=""
export myid=""
 export myval=$(expr $myval + 1 );
export email1=`echo $p | sed 's/,.*$//'`
export email2=`echo $p | sed 's/^.*,//'`
rawurlencode ${email1}; 
echo "rawemail1 = ${REPLY}"
curl -X GET -o "file${myval}.out" "https://${site}:8443/${tcpSiteStr}/api/v1/loyalty/members?member.email=${REPLY}&include=memberDetails%2CmemberAddress%2CmemberChildren%2CmemberEngagement%2CmemberBonusDayHist"  -H 'accept-language: en'  -H "authorization: bearer ${accessToken}"  -H 'content-type: application/json' 

export checkVal=`cat "file${myval}.out"  | sed 's/^.*developerMessage\(.*\)moreInfo.*$/\1/' | sed 's/^.*,"userMessage/"userMessage/' | sed 's/,.*$//' | cut -d: -f2 | sed 's/"//g'`

if [[ "${checkVal}" ==  "Success" ]]; then 
export myid=`cat "file${myval}.out"  | sed 's/^.*firstName\(.*\)isEmployee.*$/\1/' | sed 's/^.*,"id/"id/' | sed 's/,.*$//' | cut -d: -f2 | sed 's/"//g'`
curl -o "status${myval}.out" -X PATCH  "https://${site}:8443/${tcpSiteStr}/api/v1/loyalty/members/${myid}" -H 'accept-language: en'  -H "authorization: bearer ${accessToken}"  -H 'content-type: application/json' -d@- <<EOF

{ "email": "${email2}", "attributeSets": [ { "memberDetails": { "updateSource": "InStore", "updateSourceDate": "${myupddate}T00:00:00-00:00" } } ] }
EOF
export newcheckVal=`cat "status${myval}.out"  | sed 's/^.*developerMessage\(.*\)moreInfo.*$/\1/' | sed 's/^.*,"userMessage/"userMessage/' | sed 's/,.*$//' | cut -d: -f2 | sed 's/"//g'`

if [[ "${newcheckVal}" == "You have successfully updated your profile."  ]]; then
 echo "${email1}, ${myid}, ${email2}, Success,${myupddate}" >> "file${myupddate}.log"
  rm "file${myval}.out"
  rm "status${myval}.out"
else
 echo "${email1}, ${myid}, ${email2}, Failure" >> "file${myupddate}.log"
fi

else

 echo "${email1}, \"No-ID-Retreived\", ${email2}, Failure" >> "file${myupddate}.log"
fi

done <emailChange.txt

